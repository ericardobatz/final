﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using waFinal.ServiceReference1;

namespace waFinal
{
    public partial class Pagina : System.Web.UI.Page
    {
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Consultar();
                llenarDdlDe();
            }

        }

        void llenarDdlDe() {
            using (Service1Client cambio = new Service1Client())
            {
                ddlDe.DataSource = cambio.getMonedaDe();
                ddlDe.DataBind();
            }
            llenarDdlA();
        }

        protected void llenarA(object sender, EventArgs e)
        {
            llenarDdlA();
        }

        void llenarDdlA() {
            using (Service1Client cambio = new Service1Client())
            {
                ddlA.DataSource = cambio.getMonedaA(ddlDe.SelectedItem.Text);
                ddlA.DataBind();
            }
        }

        void Consultar() {
            using (Service1Client cambio = new Service1Client()) {
                gvCambio.DataSource = cambio.Consultar();
                gvCambio.DataBind();
            }
        }

        void Seleccionar()
        {
            using (Service1Client cambio = new Service1Client())
            {
                decimal monto = cambio.getCambio(ddlDe.SelectedItem.Text, ddlA.SelectedItem.Text);
                decimal aConvertir = Convert.ToDecimal(tbxCantidad.Text);
                monto = monto * aConvertir;
                string res = String.Format(tbxCantidad.Text + " " + ddlDe.SelectedItem.Text + " = " + monto.ToString() + " " + ddlA.SelectedItem.Text);
                lblRes.Text = res;
            }
        }

        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            Seleccionar();   
        }
    }
}