﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Pagina.aspx.cs" Inherits="waFinal.Pagina" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label runat="server" Text="Tabla de Equivalencias obtenida de WS"></asp:Label>
        <br />
        <div>
            <asp:GridView ID="gvCambio" runat="server" AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundField HeaderText="De" DataField="monedaDe"/>
                    <asp:BoundField HeaderText="" DataField="cantidadDe"/>
                    <asp:BoundField HeaderText="A" DataField="monedaA"/>
                    <asp:BoundField HeaderText="" DataField="cantidadA"/>
                </Columns>
            </asp:GridView>
        </div>
        <br />
        <div>
            <asp:Label runat="server" Text="Convertir"></asp:Label>
            <asp:TextBox runat="server" type="number" ID="tbxCantidad"></asp:TextBox>
            <asp:DropDownList runat="server" ID="ddlDe" OnSelectedIndexChanged="llenarA" AutoPostBack="true"></asp:DropDownList>
            <asp:Label runat="server" Text="A"></asp:Label>
            <asp:DropDownList runat="server" ID="ddlA"></asp:DropDownList>
            <asp:Button ID="btnConsultar" runat="server" Text="Convertir" OnClick="btnConsultar_Click" />
            <br />
            <br />
            <asp:Label runat="server" ID="lblRes" Text=""></asp:Label>
        </div>
    </form>
</body>
</html>
