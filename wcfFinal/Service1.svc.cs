﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using wcfFinal.Modelo;

namespace wcfFinal
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        public IEnumerable<Cambio> Consultar()
        {
            using (dbEntities contexto = new dbEntities()) 
            {
                return contexto.Cambio.AsNoTracking().ToList();
            }
        }

        public List<string> getMonedaDe()
        {
            using (dbEntities contexto = new dbEntities())
            {
                IQueryable<string> deQ = contexto.Cambio.Select(t => t.monedaDe).Distinct();
                List<string> de = deQ.ToList();
                return de;
            }
        }

        public List<string> getMonedaA(string valor)
        {
            using (dbEntities contexto = new dbEntities())
            {
                IQueryable<string> deQ = contexto.Cambio.Where(c => c.monedaDe == valor).Select(cc => cc.monedaA).Distinct();
                List<string> de = deQ.ToList();
                return de;
            }
        }

        public decimal getCambio(string de, string a)
        {
            using (dbEntities contexto = new dbEntities())
            {
                decimal monto = contexto.Cambio.Where(c => c.monedaDe == de && c.monedaA == a).Select(cc => cc.cantidadA).SingleOrDefault();
                return monto;
            }
        }

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
    }
}
